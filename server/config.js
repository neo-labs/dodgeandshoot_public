var config = {
    local: {
        mode: 'local',
        port: 8080,
        dbMongo: 'mongodb://localhost:27017/dodge_and_shoot',
        socket: {url:'http://192.168.1.116',port:9000},
        secret:'secr22_sahj34uq89ncds',
        sessionMaxAge: 36000000,
        game: {
          width: 1280,
          height: 650,
          maxPlayers: 10,
          maxHeartbeatInterval: 5000,
        },
        network:{
          updateFactor: 40,
        }
    },
    production: {
        mode: 'production',
        port: 8080,
        dbMongo: 'mongodb://localhost:27017/dodge_and_shoot',
        socket: {url:'http://192.168.1.116',port:9000},
        secret:'secr22_sahj34uq89ncds',
        sessionMaxAge: 36000000,
        game: {
          width: 5000,
          height: 5000,
          maxPlayers: 10,
        },
        network:{
          updateFactor: 40,
        }
    }
}
module.exports = function(mode) {
    return config[mode || process.argv[2] || 'local'] || config.local;
}
