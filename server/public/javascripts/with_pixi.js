window.onload = function() {
  width = 1280;
  height = 650;
  widthJoystick = 120;
  heightJoystick = 120;
  var rand = Math.floor(Math.random()*1000)
  var player = {
    uid: rand,
    id: rand,
    speed: 4,
    x:width/2,
    y:height/2,
    r:0,
    exp: 10,
    bulet: {
      power: 2,
      speed: 10,
      bulets: []
    },
    name: 'user_'+rand,
    lastHeartbeat: new Date().getTime(),
    target: {
      rotation: 0,
      angle: 0,
      speed: 0,
    },
    radius: 20,
    screenWidth: width,
    screenHeight: height,
  }
  var playerName = 'roman';
  var playerType = 'player';
  var reason;
  var gameStart = false;
  var disconnected = false;
  var died = false;
  var kicked = false;
  var playersSprite = {};
  var players = [];
  /*var comets = [];
  var cometsSprite = {}
*/

  var startPingTime = 0;
  var socket = io({query: {player: JSON.stringify(player)}});
  socket.on('pong', function () {
      var latency = Date.now() - startPingTime;
      console.log('Latency: ' + latency + 'ms');
  });
  socket.on('connect_failed', function () {
      socket.close();
      disconnected = true;
  });

  socket.on('disconnect', function () {
      socket.close();
      disconnected = true;
  });
  socket.on('welcome', function (playerSettings) {
      console.log('welcome',playerSettings)
      player = playerSettings;
      player.name = playerName;
      player.screenWidth = screenWidth;
      player.screenHeight = screenHeight;
      player.target = target;
      socket.emit('gotit', player);
      gameStart = true;
  });

  socket.on('gameSetup', function(data) {
      player.x = data.x;
      player.y = data.y;
      gameWidth = data.gameWidth;
      gameHeight = data.gameHeight;
  });

  socket.on('playerDied', function (data) {
      console.log('playerDied - ' + (data.name.length < 1 ? 'An unnamed cell' : data.name) + ' was eaten.');
  });

  socket.on('playerDisconnect', function (data) {
      console.log('playerDisconnect - ' + (data.name.length < 1 ? 'An unnamed cell' : data.name) + ' disconnected.');
  });

  socket.on('playerJoin', function (data) {
      console.log('playerJoin -' + (data.name.length < 1 ? 'An unnamed cell' : data.name) + ' joined.');
  });



  socket.on('serverMSG', function (data) {
      console.log('serverMSG',data);
  });

  console.log('showLog',showLog)
  // Handle movement.
  socket.on('serverPlay', function (userData, buletData/*, comet*/) {
      if (showLog) console.log('serverPlay',userData,buletData/*, comet*/)
      if (buletData){
        player.bulet.bulets = buletData
      }
      if (userData) {
        players = userData;
      }
//       comets[0] = comet;
  });

  // Death.
  socket.on('RIP', function () {
      gameStart = false;
      died = true;

  });

  socket.on('kick', function (data) {
      gameStart = false;
      kicked = true;
      socket.close();
  });

  socket.on('virusSplit', function (virusCell) {
      socket.emit('2', virusCell);
      reenviar = false;
  });

var renderer = PIXI.autoDetectRenderer(width, height,{backgroundColor : 0x1099bb});
document.body.appendChild(renderer.view);
var stage = new PIXI.Container();

var playerContainer = new PIXI.Container();
stage.addChild(playerContainer)
/*

var cometContainer = new PIXI.Container();
stage.addChild(cometContainer);
*/

function movePlayer (p) {
  if (showLog) console.log('movePlayer',p)
  if (p.id == player.id) {
    player.x = p.x;
    player.y = p.y;
    player.r = p.r;
  }
  if (!playersSprite[p.id] && p.life>0){
    playersSprite[p.id] = PIXI.Sprite.fromImage('/images/block.png');
    playersSprite[p.id].position.set(p.x, p.y);
    playersSprite[p.id].width = 100;
    playersSprite[p.id].height = 100;
    playersSprite[p.id].interactive = true;
    playersSprite[p.id].anchor.x = 0.5;
    playersSprite[p.id].anchor.y = 0.5;
    playersSprite[p.id].position.x = p.x;
    playersSprite[p.id].position.y = p.y;
    playersSprite[p.id].rotation = p.r;
    playerContainer.addChild(playersSprite[p.id])
  } else {
    if (p.life<=0) {
      playerContainer.removeChild(playersSprite[p.id]);
      delete playersSprite[p.id]
    } else {
      playersSprite[p.id].position.x = p.x;
      playersSprite[p.id].position.y = p.y;
      playersSprite[p.id].rotation = p.r;
    }
  }
}

/*

function moveComet (c) {
  if (showLog) console.log('moveComet',c, cometsSprite, comets)
  if (c.life>0) {
    if (!cometsSprite[c.id]){
        cometsSprite[c.id] = PIXI.Sprite.fromImage('/images/badlogic.jpg');
        cometsSprite[c.id].position.set(c.x, c.y);
        cometsSprite[c.id].width = 100;
        cometsSprite[c.id].height = 100;
        cometsSprite[c.id].interactive = true;
        cometsSprite[c.id].anchor.x = 0.5;
        cometsSprite[c.id].anchor.y = 0.5;
        cometsSprite[c.id].position.x = c.x;
        cometsSprite[c.id].position.y = c.y;
        cometsSprite[c.id].rotation = c.r;
        cometContainer.addChild(cometsSprite[c.id])
      } else {
        cometsSprite[c.id].position.x = c.x;
        cometsSprite[c.id].position.y = c.y;
      }
  } else {
    cometContainer.removeChild(cometsSprite[c.id]);
    comets = []
  }
}
*/


var controlContainer = new PIXI.Container();
stage.addChild(controlContainer);

function fireBulet(startPosition){
  console.log('startPosition',startPosition);
  player.bulet.bulets.push({id: startPosition.id, x: startPosition.x, y: startPosition.y, r: startPosition.r})
}
function fire() {
  var center = {x: width-widthJoystick/2, y : height-heightJoystick*2 }
  var spriteFire = PIXI.Sprite.fromImage('/images/touchKnob.png');
  spriteFire.position.set(center.x,center.y);
  spriteFire.width = widthJoystick/1.5;
  spriteFire.height = heightJoystick/1.5;
  spriteFire.anchor.x = 0.5
  spriteFire.anchor.y = 0.5
  spriteFire.interactive = true;
  spriteFire.on('mousedown', onDown);
  spriteFire.on('touchstart', onDown);

  function onDown (eventData) {
      // players[player.id].scale.x += 0.1;
      // players[player.id].scale.y += 0.1;
      socket.emit('fire', {playerId: player.id, x: player.x, y: player.y, angle:  player.r})
      /*
      setTimeout(function(){
        players[player.id].scale.x = 1
        players[player.id].scale.y = 1
      },100)
      */
  }
  controlContainer.addChild(spriteFire)

}
fire()


function rightJoystick(){
  var center = {x: width-widthJoystick, y : height-heightJoystick }
  var spriteJoystick = PIXI.Sprite.fromImage('/images/touchBackground.png');
  spriteJoystick.position.set(center.x,center.y);
  spriteJoystick.width = widthJoystick;
  spriteJoystick.height = heightJoystick;
  spriteJoystick.interactive = false;
  spriteJoystick.anchor.x = 0.5
  spriteJoystick.anchor.y = 0.5
  controlContainer.addChild(spriteJoystick);


  var spriteJoystickTouch = PIXI.Sprite.fromImage('/images/touchKnob.png');
  spriteJoystickTouch.position.set(width-widthJoystick,height-heightJoystick);
  spriteJoystickTouch.width = widthJoystick/3;
  spriteJoystickTouch.height = heightJoystick/3;
  spriteJoystickTouch.interactive = true;
  spriteJoystickTouch.anchor.x = 0.5;
  spriteJoystickTouch.anchor.y = 0.5;
  spriteJoystickTouch.on('mousedown', onDragStart)
        .on('touchstart', onDragStart)
        .on('mouseup', onDragEnd)
        .on('mouseupoutside', onDragEnd)
        .on('touchend', onDragEnd)
        .on('touchendoutside', onDragEnd)
        .on('mousemove', onDragMove)
        .on('touchmove', onDragMove);
  controlContainer.addChild(spriteJoystickTouch);


  function onDragStart(event)
  {
      this.data = event.data;
      this.alpha = 0.5;
      this.dragging = true;
  }

  function onDragEnd()
  {
      this.alpha = 1;
      this.dragging = false;
      this.data = null;
      this.position.x = center.x;
      this.position.y = center.y;
//      player.target.rotation = 0;
      player.target.rotationSpeed = 0;
  }

  function onDragMove()
  {
      if (this.dragging)
      {
          var newPosition = this.data.getLocalPosition(this.parent);
          this.position.x = newPosition.x;
          this.position.y = newPosition.y;
          var angle = Math.atan(center.y - newPosition.y, center.x - newPosition.x)
          player.target.rotation = -angle;
          player.target.rotationSpeed = 1;
          console.log('angle',angle)
      }
  }
}

rightJoystick();

function leftJoystick(){
  var center = {x: widthJoystick, y : height-heightJoystick }

  var spriteJoystick = PIXI.Sprite.fromImage('/images/touchBackground.png');
  spriteJoystick.position.set(center.x,center.y);
  spriteJoystick.width = widthJoystick;
  spriteJoystick.height = heightJoystick;
  spriteJoystick.interactive = false;
  spriteJoystick.anchor.x = 0.5
  spriteJoystick.anchor.y = 0.5
  controlContainer.addChild(spriteJoystick);


  var spriteJoystickTouch = PIXI.Sprite.fromImage('/images/touchKnob.png');
  spriteJoystickTouch.position.set(widthJoystick,height-heightJoystick);
  spriteJoystickTouch.width = widthJoystick/3;
  spriteJoystickTouch.height = heightJoystick/3;
  spriteJoystickTouch.interactive = true;
  spriteJoystickTouch.anchor.x = 0.5
  spriteJoystickTouch.anchor.y = 0.5
  spriteJoystickTouch.on('mousedown', onDragStart)
        .on('touchstart', onDragStart)
        .on('mouseup', onDragEnd)
        .on('mouseupoutside', onDragEnd)
        .on('touchend', onDragEnd)
        .on('touchendoutside', onDragEnd)
        .on('mousemove', onDragMove)
        .on('touchmove', onDragMove);
  controlContainer.addChild(spriteJoystickTouch);

  function onDragStart(event)
  {
      console.log('onDragStart',event.data)
      this.data = event.data;
      this.alpha = 0.5;
      this.dragging = true;
  }

  function onDragEnd()
  {
      console.log('onDragEnd')
      this.alpha = 1;

      this.dragging = false;
      this.data = null;
      this.position.x = center.x;
      this.position.y = center.y;
      // player.target.x = 0;
      // player.target.y = 0;
      player.target.angle = 0;
      player.target.speed = 0;
  }

  function onDragMove()
  {
      if (this.dragging)
      {
          var newPosition = this.data.getLocalPosition(this.parent);
          this.position.x = newPosition.x;
          this.position.y = newPosition.y;
          var angle = Math.atan2(center.y - newPosition.y, center.x - newPosition.x)
          player.target.angle = angle;
          player.target.speed = 1;
          console.log('onDragMove',angle, 1)
      }
  }
}

leftJoystick()

animate();
var buletContainer = new PIXI.Container();
stage.addChild(buletContainer);
var bulets = {};
function moveBulet (b){
  if (showLog) console.log('moveBulet ',b)
  if (!bulets[b.id] && b.life>0){
    bulets[b.id] = PIXI.Sprite.fromImage('/images/touchKnob.png');
    bulets[b.id].position.set(b.x, b.y);
    bulets[b.id].width = 15;
    bulets[b.id].height = 15;
    bulets[b.id].interactive = false;
    bulets[b.id].anchor.x = 0.5;
    bulets[b.id].anchor.y = 0.5;
    buletContainer.addChild(bulets[b.id])
  } else {
    if (b.life<=0) {
      buletContainer.removeChild(bulets[b.id]);
      delete bulets[b.id]
    } else {

      bulets[b.id].position.x = b.x;
      bulets[b.id].position.y = b.y;
    }
  }
}

function flushBulet(){

}

function animate() {
    requestAnimationFrame(animate);
    /*
    sprite.position.x = player.x;
    sprite.position.y = player.y;
    if (sprite.position.x> width) {
      sprite.position.x = width
    }
    if (sprite.position.x < 0) {
      sprite.position.x = 0
    }
    if (sprite.position.y > height) {
      sprite.position.y = height
    }
    if (sprite.position.y < 0) {
      sprite.position.y = 0
    }
    sprite.rotation = player.r;
    */

    if (players && players.length>0){
      players.map(function(p){
        movePlayer(p)
      })
    }
    if (player.bulet && player.bulet.bulets && player.bulet.bulets.length>0){
      player.bulet.bulets.map(function(f){
        moveBulet(f)
      })
    }
    /*
    if (comets && comets.length===1){
      // if (showLog) console.log('comets',comets)
      moveComet(comets[0])
    }*/
    socket.emit('0', JSON.stringify(player.target));
    renderer.render(stage);
}
}
