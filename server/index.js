var express = require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var cookie = require('cookie');
var moment = require('moment');
var quadtree = require('./libs/quadtree')
var sat = require('sat');
var util = require('./libs/util');
var config = require('./config')();
//var session = require('express-session');
//var MongoStore = require('connect-mongo')(session);
//var sessionStore = new MongoStore({url:config.dbMongo});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res){
  res.render('index', { title: 'Express' });
});
app.get('/api', function(req, res){
  res.send( {status:1,  data: {} });
});

var args = { x: 0, y: 0, h: config.game.height, w: config.game.width, maxChildren: 1, maxDepth: 5 };
var tree = quadtree.QUAD.init(args);
var players = [];
var playerConfig = {
  life: 10,
  radius: 50,
  speed: 5,
  speedRotation: 0.1
}
var bulets = [];
var buletConfig = {
  speed: 10,
  power: 2,
  life: 100
}
var sockets = {};
var leaderboard = [];
var leaderboardChanged = false;
var speed = 4;
var limitRotation = 0.1;
var V = sat.Vector;
var C = sat.Circle;
function limitSpeed(player) {
	if (player.speed > 6.25)
   		player.speed -= 0.5;
}
/*
var cometConfig = {
  life: 150,
  speed: 10,
  radius: 10,
  x: 0,
  y: 0,
  angle: -2.5
}

var comet = {};*/

function limitSpeedRotation(rotation, rotationSpeed) {
  if (rotation>0) {
    if (rotation > playerConfig.speedRotation) {
      	rotation = playerConfig.speedRotation;
    }
  } else {
    if (rotation < -playerConfig.speedRotation) {
      	rotation = -playerConfig.speedRotation;
    }
  }
  return rotation * rotationSpeed;
}


function addBulet(bulet) {
  console.log('addBulet',bulet)
    bulets.push({
        id: ((new Date()).getTime() + '-' + Math.floor(Math.random()*10000)) >>> 0,
        x: bulet.x,
        y: bulet.y,
        playerId: bulet.playerId,
        angle: bulet.angle,
        speed: buletConfig.speed,
        power: buletConfig.power,
        life: buletConfig.life
    });
}
/*
function addComet(){
    comet = Object.assign({},{id:((new Date()).getTime() + '' + Math.floor(Math.random()*1000)) >>> 0}, cometConfig);
    // console.log('addComet', comet)
}*/
/*

function moveComet(){
  if (Object.getOwnPropertyNames(comet).length > 0) {
    var deltaY = comet.speed * Math.sin(-comet.angle);
    var deltaX = comet.speed * Math.cos(comet.angle);
    if (!isNaN(deltaY)) {
        comet.y += deltaY;
    }
    if (!isNaN(deltaX)) {
        comet.x -= deltaX;
    }
    comet.life-=1;
    if (comet.life<=-1) {
      comet = {};
    }
    // console.log('comet',comet)
  }
}
*/

function movePlayer(player) {
    if (player && player.target) {
        var target = {
          angle: player.target.angle,
          speed: player.target.speed,
          rotation: player.target.rotation,
          rotationSpeed: player.target.rotationSpeed
        };
        var deltaY = target.speed * playerConfig.speed * Math.sin(target.angle);
        var deltaX = target.speed * playerConfig.speed * Math.cos(target.angle);
        //var rotation = limitSpeedRotation(target.rotation, target.rotationSpeed);

        if (!isNaN(deltaY)) {
            player.y -= deltaY;
        }

        if (!isNaN(deltaX)) {
            player.x += deltaX;
        }

        if (!isNaN(target.rotation)) {
            player.r = ((target.rotationSpeed==0)?(player.r):(target.rotation));
        }

        var borderCalc = player.radius / 3;
        if (player.x > config.game.width - borderCalc) {
            player.x = config.game.width - borderCalc;
        }
        if (player.y > config.game.height - borderCalc) {
            player.y = config.game.height - borderCalc;
        }
        if (player.x < borderCalc) {
            player.x = borderCalc;
        }
        if (player.y < borderCalc) {
            player.y = borderCalc;
        }
    }
}


function moveloop() {
  // console.log('moveloop')
    // дижения игроков
    players.forEach(function(p,i,ps){
      if (p.life>0) {
        tickPlayer(p);
      } else {
        ps.splice(i,1);
      }
    })
    // выстрелами
    bulets.forEach(function (b,i,bs){
      //console.log('bulets.forEach', bulets)
      if (b.life>0) {
        moveBulet(b);
      } else {
        // удалим
        bs.splice(i,1);
      }
    })

    //пускаем кометы
//    moveComet();

    // вычисляем попадания
    calcCollision();
}

function tickPlayer(currentPlayer) {
    // если время жизни истекло или жизни закончились у игрока
    if (currentPlayer.lastHeartbeat < new Date().getTime() - config.game.maxHeartbeatInterval ) {
        sockets[currentPlayer.id].emit('kick', {status: 1, data:{}, message:'Last heartbeat received over ' + config.game.maxHeartbeatInterval + ' ago.'});
        sockets[currentPlayer.id].disconnect();
    }
    movePlayer(currentPlayer);
}

function moveBulet(curBulet) {
  if (curBulet.life>0) {
    var target = {
      angle: curBulet.angle,
      speed: curBulet.speed,
      life: curBulet.life,
      power: curBulet.power
    };
    var deltaY = target.speed * Math.sin(target.angle);
    var deltaX = target.speed * Math.cos(target.angle);
    if (!isNaN(deltaY)) {
        curBulet.y -= deltaY;
    }
    if (!isNaN(deltaX)) {
        curBulet.x += deltaX;
    }
    curBulet.life--;
  }
}


function calcCollision () {
  players.forEach(function(p) {
    bulets.forEach(function(b) {
//      console.log('inCircle(p, b)',p,b)
      if (inCircle(p, b)) {
        // если пуля и игрок живы
        if (b.life > 0 && b.playerId != p.id && p.life > 0) {
          // уменьшаем кол-во жизней игрока, в которого она попала
          p.life -= b.power;
          // убиваем пулю
          b.life = 0;
        }
      }
    })
  })
}
function inCircle(player, bulet) {
  return Math.pow((player.x-bulet.x),2) + Math.pow((player.y-bulet.y),2) <= Math.pow(playerConfig.radius,2);
}

function sendUpdates() {
    players.forEach(function (u) {
        u.x = u.x || config.game.width / 2;
        u.y = u.y || config.game.height / 2;

        var visibleCells = players.map(function (f) {
            /*if (f.x + f.radius > u.x - u.screenWidth / 2 - 20 &&
                f.x - f.radius < u.x + u.screenWidth / 2 + 20 &&
                f.y + f.radius > u.y - u.screenHeight / 2 - 20 &&
                f.y - f.radius < u.y + u.screenHeight / 2 + 20) {
                return f;
            }*/
            return f;
        }).filter(function (f) {
            return f;
        });


        var visibleBulets = bulets.map(function (f) {
/*
            if (f.x + f.radius > u.x - u.screenWidth / 2 - 20 &&
                f.x - f.radius < u.x + u.screenWidth / 2 + 20 &&
                f.y + f.radius > u.y - u.screenHeight / 2 - 20 &&
                f.y - f.radius < u.y + u.screenHeight / 2 + 20) {
                return f;
            }
            */
            return f;
        }).filter(function (f) {
            return f;
        });
        sockets[u.id].emit('serverPlay', visibleCells, visibleBulets/*, comet*/);
    });
}


io.on('connection', function (socket) {
  var query = {};
  console.log('[INFO] connection  query:', socket.handshake.query.player, JSON.parse(socket.handshake.query.player))
  try {
    query = JSON.parse(socket.handshake.query.player);

  } catch (e) {
    console.log('[ERROR] connection ', e);
  }
  // var query = JSON.parse(socket.handshake.socket.handshake.player) || {};
  var position = util.randomPosition(query.radius, query.screenWidth, query.screenHeight);
  var currentPlayer = {
      uid: query.id,
      id: query.id,
      x: position.x,
      y: position.y,
      r: 0, // rotation
      bulet: query.bulet,
      name: query.name,
      life: playerConfig.life,
      lastHeartbeat: new Date().getTime(),
      target: { angle: 0, speed:0, rotation: 0 },
      radius: query.radius,
      screenWidth: query.screenWidth,
      screenHeight: query.screenHeight,
  };
  console.log('[INFO] Player ' + currentPlayer.name + ' connecting...');
  // проверка на существование игрока
  var playerIndex = util.findIndex(players, currentPlayer.id);
  if (playerIndex < 0) {
      if (util.validNick(currentPlayer.name)) {
          console.log('[INFO] Player ' + currentPlayer.name + ' connected!');
          sockets[currentPlayer.id] = socket;
          players.push(currentPlayer);
//          socket.emit('playerJoin', currentPlayer);
          socket.broadcast.emit('playerJoin', currentPlayer);
          console.log('socket.emit.gameSetup',currentPlayer);
          socket.emit('gameSetup', Object.assign ({},{gameWidth: config.game.width, gameHeight: config.game.height}, currentPlayer));
          console.log('[INFO] Total players: ' + players.length);

      } else {
          console.log('[INFO] Player ' + currentPlayer.name + ' kick!');
          socket.emit('kick', 'Invalid username.');
          socket.disconnect();
      }
  } else {
      console.log('[INFO] Player ID is already connected, kicking.');
      socket.emit('kick', 'Player ID is already connected.');
      socket.disconnect();
  }
  socket.on('ping', function () {
      console.log('ping');
      socket.emit('pong');
  });
  socket.on('respawn', function () {
      var playerIndex = util.findIndex(players, currentPlayer.id);
      if (playerIndex > -1)
          players.splice(playerIndex, 1);
      socket.emit('welcome', currentPlayer);
      console.log('[INFO] User ' + currentPlayer.name + ' respawned!');
  });

  socket.on('disconnect', function () {
      var playerIndex = util.findIndex(players, currentPlayer.id);
      if (playerIndex > -1)
          players.splice(playerIndex, 1);
      socket.broadcast.emit('playerDisconnect', { id: currentPlayer.id });
      console.log('[INFO] User ' + currentPlayer.name + ' disconnected!');
      console.log('[INFO] Total players: ' + players.length);
  });
//
  socket.on('0', function (target) {
    try {
        //console.log('socket.on(0)', target)
        currentPlayer.lastHeartbeat = new Date().getTime();
        currentPlayer.target = JSON.parse(target);
    } catch (e) {
        console.log('[ERROR] socket.on(0)',e)
    }

  });

  socket.on('fire', function(bulet) {
//    console.log('fire', bulet)
    addBulet( JSON.parse(bulet))
  })
})

setInterval(moveloop, 1000 / 60);
setInterval(sendUpdates, 1000 / config.network.updateFactor);
//setInterval(addComet, 5000);


http.listen(3000, function(){
  console.log('listening on *:3000');
});
