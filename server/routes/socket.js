// var express = require('express');
// var router = express.Router();
// var app = module.exports = express();


var express = require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var cookie = require('cookie');
var moment = require('moment');
var quadtree = require('../libs/quadtree')
var sat = require('sat');
var util = require('../libs/util');
var config = require('../config')();
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var sessionStore = new MongoStore({url:config.dbMongo});


var args = { x: 0, y: 0, h: config.game.height, w: config.game.width, maxChildren: 1, maxDepth: 5 };
var tree = quadtree.QUAD.init(args);
var players = [];
var bulets = [];
var sockets = {};
var leaderboard = [];
var leaderboardChanged = false;
var V = sat.Vector;
var C = sat.Circle;
function limitSpeed(player) {
	if (player.speed > 6.25)
   		player.speed -= 0.5;
}

function movePlayer(player) {
    var target = { x: player.target.x, y: player.target.y, r: player.target.r };

    var angle = Math.atan2(target.y, target.x);

    var deltaY = player.speed * Math.sin(angle) ;
    var deltaX = player.speed * Math.cos(angle) ;
    var rotation = target.r;

    if (!isNaN(deltaY)) {
        player.y += deltaY;
    }

    if (!isNaN(deltaX)) {
        player.x += deltaX;
    }

      if (!isNaN(rotation)) {
        player.r += rotation;
    }

    // limit player on game field
    var borderCalc = player.radius / 3;
    if (player.x > config.game.width - borderCalc) {
        player.x = config.game.width - borderCalc;
    }
    if (player.y > config.game.height - borderCalc) {
        player.y = config.game.height - borderCalc;
    }
    if (player.x < borderCalc) {
        player.x = borderCalc;
    }
    if (player.y < borderCalc) {
        player.y = borderCalc;
    }
}

function moveloop() {
    for (var i = 0; i < players.length; i++) {
        tickPlayer(players[i]);
    }
}

function sendUpdates() {
    players.forEach(function (u) {
        // center the view if x/y is undefined, this will happen for spectators
        u.x = u.x || config.game.width / 2;
        u.y = u.y || config.game.height / 2;

        var visibleCells = players.map(function (f) {
            if (f.x + f.radius > u.x - u.screenWidth / 2 - 20 &&
                f.x - f.radius < u.x + u.screenWidth / 2 + 20 &&
                f.y + f.radius > u.y - u.screenHeight / 2 - 20 &&
                f.y - f.radius < u.y + u.screenHeight / 2 + 20) {
                if (f.id !== u.id) {
                    return {
                        id: f.id,
                        x: f.x,
                        y: f.y,
                        hue: f.hue,
                        name: f.name
                    };
                } else {
                    return {
                        x: f.x,
                        y: f.y,
                        hue: f.hue
                    };
                }
            }
        }).filter(function (f) {
            return f;
        });
        sockets[u.id].emit('serverTellPlayerMove', visibleCells);
    });
}

//
// var io = require('socket.io').listen(config.socket.port, function () {
//     log.info('Server is listening on  '+config.socket.url + ':' + config.socket.port);
// });

io.on('connection', function (socket) {

  var query = JSON.parse(socket.handshake.query.player);
  var position = util.randomPosition(query.radius);
  var currentPlayer = {
      uid: query.uid,
      id: socket.id,
      x: position.x,
      y: position.y,
      r: 0, // rotation
      model: query.model,
      exp: query.exp,
      bulet: query.bulet,
      radius: query.radius,
      name: query.name,
      lastHeartbeat: new Date().getTime(),
      target: { x: 0, y: 0, r: 0 },
      screenWidth: query.screenWidth,
      screenHeight: query.screenHeight,
  };
  console.log('[INFO] Player ' + currentPlayer.name + ' connecting...');
  // проверка на существование игрока
  var playerIndex = util.findIndex(players, currentPlayer.id);
  if (playerIndex < 0) {
      if (util.validNick(currentPlayer.name)) {
          console.log('[INFO] Player ' + currentPlayer.name + ' connected!');
          sockets[currentPlayer.id] = socket;
          players.push(currentPlayer);
          io.emit('playerJoin', { name: currentPlayer.name });
          socket.emit('gameSetup', {
              positionX : currentPlayer.x,
              positionY : currentPlayer.y,
              gameWidth: config.game.width,
              gameHeight: config.game.height
          });

          console.log('[INFO] Total players: ' + players.length);

      } else {
          socket.emit('kick', 'Invalid username.');
          socket.disconnect();
      }
  } else {
      console.log('[INFO] Player ID is already connected, kicking.');
      socket.emit('kick', 'Player ID is already connected.');
      socket.disconnect();
  }
  socket.on('ping', function () {
      socket.emit('pong');
  });
  socket.on('respawn', function () {
      var playerIndex = util.findIndex(players, currentPlayer.id);
      if (playerIndex > -1)
          players.splice(playerIndex, 1);
      socket.emit('welcome', currentPlayer);
      console.log('[INFO] User ' + currentPlayer.name + ' respawned!');
  });

  socket.on('disconnect', function () {
      var playerIndex = util.findIndex(players, currentPlayer.id);
      if (playerIndex > -1)
          players.splice(playerIndex, 1);
      socket.broadcast.emit('playerDisconnect', { name: currentPlayer.name });
      console.log('[INFO] User ' + currentPlayer.name + ' disconnected!');
      console.log('[INFO] Total players: ' + players.length);
  });

  socket.on('0', function (target) {
      console.log('[INFO] Heartbeat.');
      currentPlayer.lastHeartbeat = new Date().getTime();
      if (target.x !== currentPlayer.x || target.y !== currentPlayer.y || target.r !== currentPlayer.r ) {
          currentPlayer.target = target;
      }
  });
})


setInterval(moveloop, 1000 / 60);
setInterval(sendUpdates, 1000 / config.network.updateFactor);
