var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.send({ status: 1, data: {} });
});

module.exports = router;
