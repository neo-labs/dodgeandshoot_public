package com.appcraft.sprite;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class BulletSprite extends Sprite {
    public BulletSprite(Texture texture){
        super(texture);
    }
}
