package com.appcraft.screen;

import com.appcraft.MainGame;
import com.appcraft.api.SocketApi;
import com.appcraft.model.Bullet;
import com.appcraft.model.Player;
import com.appcraft.model.Target;
import com.appcraft.sprite.PlayerSprite;
import com.appcraft.sprite.BulletSprite;
import com.appcraft.utils.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class GameScreen implements Screen {

    private OrthographicCamera camera;
    private Stage stage;
    private Touchpad rightJoystick;
    private Touchpad leftJoystick;
    private TiledMap tiledMap;
    private TiledMapRenderer tiledMapRenderer;
    private Target target;
    private Timer.Task hardbeat;
    private Timer timer;
    private Gson gson;
    private String myId;
    private Player myPlayer;
    Texture playerTexture;
    Texture bulletTexture;
    HashMap<String, PlayerSprite> players;
    HashMap<String, BulletSprite> bullets;
    private MainGame game;
    private long dateLastBullet;
    private int resolutionPlayer = 96;
    private int resolutionBullet = 30;

    public GameScreen (final MainGame gam) {
        game = gam;
    }


    @Override
    public void show() {
        initGame();
        initCamera();
        initJoysticks();
        createStage();
        createCharacter();
    }

    public void initGame() {
        this.target = new Target();
        myPlayer = new Player();
        this.gson = new Gson();

        initTimer();

        SocketApi.getInstance().getSocket()
                .on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        timer.start();
                    }
                })
                .on("gameSetup", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            System.out.println("gameSetup" + data);
                            String newId = data.getString("id");
                            myPlayer.setId(newId);
                            dateLastBullet=System.currentTimeMillis();
                        } catch (Exception e) {
                            System.out.println("gameSetup" + e);
                        }
                    }
                }).on("playerJoin", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject data = (JSONObject) args[0];
                    String newId = data.getString("id");
                    players.put(newId, new PlayerSprite(playerTexture));
                    System.out.println("playerJoin OK: " + newId);
                } catch (Exception e) {
                    System.out.println("playerJoin ERROR:" + e);
                }
            }
        }).on("serverPlay", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                movePlayers((JSONArray) args[0]);
                moveBullet((JSONArray) args[1]);
            }
        }).on("pong", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    String pong = args[0].toString();
                    System.out.println("pong OK: " + pong);
                } catch (Exception e) {
                    System.out.println("pong" + e);
                }
            }
        }).on("playerDisconnect", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    String delId = data.getString("id");
                    players.remove(delId);
                    System.out.println("playerDisconnect OK: " + delId);
                } catch (JSONException e) {
                    System.out.println("playerDisconnect ERROR: " + e);
                }
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    String eventDisconnect = args[0].toString();
                    System.out.println(Socket.EVENT_DISCONNECT + " OK: " + eventDisconnect);
                } catch (Exception e) {
                    System.out.println(Socket.EVENT_DISCONNECT + " ERROR: " + e);
                }
            }
        });
        SocketApi.getInstance().connect();
    }


    private void initTimer() {
        this.timer = new Timer();
        hardbeat = new Timer.Task() {
            @Override
            public void run() {
                SocketApi.getInstance().getSocket().emit("0", gson.toJson(target));
            }
        };
        this.timer.scheduleTask(hardbeat, 0, Constants.HARDBEAT_INTERAVAL);
    }




    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 1, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (leftJoystick.getKnobPercentY() == 0 && leftJoystick.getKnobPercentX() == 0) {
            target.setAngle(0);
            target.setSpeed(0);
        } else {
            double angle = Math.atan2(leftJoystick.getKnobPercentY(), leftJoystick.getKnobPercentX());
            target.setAngle(angle);
            target.setSpeed(1);
        }
        if (rightJoystick.getKnobPercentY() == 0 && rightJoystick.getKnobPercentX() == 0) {
            target.setRotationSpeed(0);
        } else {
            double angleRotation = Math.atan2(rightJoystick.getKnobPercentY(), rightJoystick.getKnobPercentX());
            target.setRotation(angleRotation);
            target.setRotationSpeed(1);
            myPlayer.setR(angleRotation);
            // ограничимся 2мя выстрелами в секунду
            if (dateLastBullet+500<System.currentTimeMillis()) {
                System.out.println("1");
                dateLastBullet = System.currentTimeMillis();
                Bullet bullet = new Bullet();
                bullet.setAngle(angleRotation);
                bullet.setPlayerId(myPlayer.getId());
                bullet.setX(myPlayer.getX());
                bullet.setY(myPlayer.getY());
                SocketApi.getInstance().getSocket().emit("fire", gson.toJson(bullet));
            }
        }
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render();
        game.batch.begin();
        for (HashMap.Entry<String, PlayerSprite> entry : players.entrySet()) {
            entry.getValue().draw(game.batch);
        }
        for (HashMap.Entry<String, BulletSprite> entry : bullets.entrySet()) {
            entry.getValue().draw(game.batch);
        }
        game.batch.end();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }


    private void initCamera() {
        camera = new OrthographicCamera((float) Gdx.graphics.getWidth(), (float) Gdx.graphics.getHeight());
        float aspectRatio = (float) Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight();
        camera.setToOrtho(true, 400f * aspectRatio, 400f);
        camera.position.set(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, 0);
        camera.update();
        tiledMap = new TmxMapLoader().load("test_map.tmx");
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
    }

    private void initJoysticks() {
        Skin skin = new Skin();
        skin.add("touchBackground", new Texture("touchBackground.png"));
        skin.add("touchKnob", new Texture("touchKnob.png"));
        Touchpad.TouchpadStyle style = new Touchpad.TouchpadStyle();
        Drawable touchBackground = skin.getDrawable("touchBackground");
        Drawable touchKnob = skin.getDrawable("touchKnob");
        style.background = touchBackground;
        style.knob = touchKnob;
        leftJoystick = new Touchpad(0, style);
        leftJoystick.setBounds(25, 25, 300, 300);
        rightJoystick = new Touchpad(0, style);
        rightJoystick.setBounds((Gdx.graphics.getWidth() - 300 - 25), 25, 300, 300);

/*
        Skin skinFire = new Skin();
        TextureAtlas buttonAtlas = new TextureAtlas(Gdx.files.internal("images.pack"));
        skinFire.addRegions(buttonAtlas);
        TextButton.TextButtonStyle styleButton = new TextButton.TextButtonStyle();
        styleButton.font = game.font;
        styleButton.up = skinFire.getDrawable("test01");

        TextButton button = new TextButton("START", styleButton);
        button.setHeight(150);
        button.setWidth(150);

        button.setPosition(100, 100);

        button.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("button");
                Gdx.input.vibrate(20);
                return true;
            }
            ;
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.exit();
                dispose();
            }
            ;
        });*/

    }

    private void createStage() {
        Viewport viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(viewport, game.batch);
        stage.addActor(leftJoystick);
        stage.addActor(rightJoystick);
        Gdx.input.setInputProcessor(stage);
    }

    private void createCharacter() {
        playerTexture = new Texture(Gdx.files.internal("block.png"));
        players = new HashMap<String, PlayerSprite>();
        bulletTexture = new Texture(Gdx.files.internal("bullet.png"));
        bullets = new HashMap<String, BulletSprite>();
    }

    private void movePlayers(JSONArray data) {
        try {
            Skin skinProgressBar = new Skin();
            myId = myPlayer.getId();
            for (int i = 0; i < data.length(); i++) {
                PlayerSprite coopPlayer = new PlayerSprite(playerTexture);

                Vector2 position = new Vector2();
                position.x = ((Double) data.getJSONObject(i).getDouble("x")).floatValue();
                position.y = ((Double) data.getJSONObject(i).getDouble("y")).floatValue();
                float r = ((Double) data.getJSONObject(i).getDouble("r")).floatValue();
                String life = (String) data.getJSONObject(i).getString("life");
                String playerId = (String) data.getJSONObject(i).getString("id");
                coopPlayer.setPosition(position.x - resolutionPlayer/2, position.y - resolutionPlayer/2);
                float angle = (float) ((r*180)/Math.PI);
                coopPlayer.setRotation(-angle);
//                game.font.draw(game.batch, life, position.x - resolutionPlayer/2, position.y - resolutionPlayer/2);
                players.put(playerId, coopPlayer);
                if (myId.equals(playerId)) {
                    if (camera.position.x != position.x || camera.position.y !=position.y) {
                        float deltaX = (camera.position.x-position.x)/4;
                        float deltaY = (camera.position.y-position.y)/4;
                        camera.position.set(position.x + deltaX, position.y + deltaY, 0);
//                        camera.zoom += 0.01;
                    }
/*
                    else {
                        camera.zoom -= 0.01;
                    }
                    if (camera.zoom >2) {
                        camera.zoom = 2;
                    } else if (camera.zoom<1) {
                        camera.zoom = 1;
                    }*/
                    myPlayer.setX(position.x);
                    myPlayer.setY(position.y);
//                    camera.lookAt(position.x, position.y, 0);

                }
            }
        } catch (Exception e) {
            System.out.println("movePlayers ERROR:" + e);
        }
    }

    private void moveBullet(JSONArray data){
        try {
            for (int i = 0; i < data.length(); i++) {
                BulletSprite coopBullet = new BulletSprite(bulletTexture);
                Vector2 position = new Vector2();
                position.x = ((Double) data.getJSONObject(i).getDouble("x")).floatValue();
                position.y = ((Double) data.getJSONObject(i).getDouble("y")).floatValue();
                String bulletId = (String) data.getJSONObject(i).getString("id");
                coopBullet.setPosition(position.x - resolutionBullet/2, position.y - resolutionBullet/2);
                bullets.put(bulletId, coopBullet);
            }
        } catch (Exception e) {
            System.out.println("bulletPlayers ERROR:" + e);
        }
    }


}
