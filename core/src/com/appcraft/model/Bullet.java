package com.appcraft.model;

/**
 * Created by elenavlasova on 29.04.16.
 */
public class Bullet {
    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    private String playerId;
    private double x;
    private double y;
    private double angle;
    public Bullet() {
        this.angle = getAngle();
        this.x = getX();
        this.y = getY();
        this.playerId = getPlayerId();
    }
}
