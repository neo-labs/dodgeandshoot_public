package com.appcraft.model;

import com.badlogic.gdx.Gdx;

public class Player {
    private String uid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;
    private double x;
    private double y;
    private double r;
    private int life;
    private int bulet;
    private int radius;
    private String name;
    private Target target;
    private int screenWidth;
    private int screenHeight;

    public Player() {
        int rand = (int) Math.floor(Math.random()*10000);
        this.uid = String.valueOf(rand);
        this.id = String.valueOf(rand);
        this.x = 0;
        this.y = 0;
        this.r = 0;
        this.target = target;
        this.life = 0;
        this.bulet = 0;
        this.radius = 30;
        this.name = "user_" + String.valueOf(rand);
        this.screenWidth = Gdx.graphics.getWidth();
        this.screenHeight = Gdx.graphics.getHeight();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getBulet() {
        return bulet;
    }

    public void setBulet(int bulet) {
        this.bulet = bulet;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}