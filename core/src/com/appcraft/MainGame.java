package com.appcraft;

import com.appcraft.api.SocketApi;
import com.appcraft.model.GameSetup;
//import com.appcraft.model.Target;
import com.appcraft.screen.GameScreen;
//import com.appcraft.utils.Constants;
import com.appcraft.screen.MenuScreen;
import com.appcraft.utils.IMainActivity;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Timer;


public class MainGame extends Game {

    private GameScreen gameScreen;
    private MenuScreen menuScreen;
//    private MainGame mainGame;
    private IMainActivity activity;
    public SpriteBatch batch;
    public BitmapFont font;
    private static final String FONT_CHARACTERS = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.;,{}\"´`'<>";


    public MainGame(IMainActivity activity) {
        this.activity = activity;
    }



    @Override
    public void create() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Russo_One.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator.FreeTypeFontParameter();
        param.size = Gdx.graphics.getHeight() / 18; // Размер шрифта. Я сделал его исходя из размеров экрана. Правда коряво, но вы сами можете поиграться, как вам угодно.
        param.characters = FONT_CHARACTERS; // Наши символы
        font = generator.generateFont(param); // Генерируем шрифт
        param.size = Gdx.graphics.getHeight() / 20;
        font.setColor(Color.WHITE); // Цвет белый
        generator.dispose(); // Уничтожаем наш генератор за ненадобностью.
        batch = new SpriteBatch();
        this.setScreen(new MenuScreen(this));
    }

    @Override
    public void render() {
//        gameScreen.render(Gdx.graphics.getDeltaTime());
        super.render();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {

    }


}
