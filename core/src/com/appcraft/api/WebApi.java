package com.appcraft.api;

import com.appcraft.utils.Constants;

import retrofit.RestAdapter;

//  String id = activity.getAndroidId();
public class WebApi {

    // region singleton
    public static final WebApi instance = new WebApi();

    private WebApi() {
        initConnection();
    }

    public static WebApi getInstance() {
        return instance;
    }
    // endregion

    //region поля и акцессоры
    private WebInterface service;
    //endregion

    private void initConnection() {
        try {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.END_POINT_WEB)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            service = restAdapter.create(WebInterface.class);
        } catch (Throwable ignored) {
        }
    }


}
